delete from product;

insert into product
    (name, description, price, pictures)
values
    ('Product 1', 'Amazing product', 10.00, 'https://www.2checkout.com/upload/images/graphic_product_tangible.png');

insert into product
    (name, description, price, current_price, pictures)
values
    ('Product 2', 'More Amazing product', 10.00, 9.99, 'https://www.2checkout.com/upload/images/graphic_product_tangible.png');