import { Entity, PrimaryGeneratedColumn, Column, Double } from "typeorm";

@Entity()
export class Product {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    name: string;
    @Column()
    description: string;
    @Column({type: "double"})
    price: Double;
    @Column({type: "double", nullable: true})
    current_price?: Double;
    @Column("simple-array")
    pictures: string[];
}
