import { injectedServices } from "../server";

export default {
    list: async (req, res, next) => {
        const { productService } = req.injected as injectedServices;
        const products = await productService.findProducts(req.query);
        res.send(products);
    },
    count: async (req, res, next) => {
        const { productService } = req.injected as injectedServices;
        const products = await productService.countProducts(req.query);
        res.send({products});
    },
}