import "reflect-metadata";
import { createConnection } from "typeorm";
import server from "./server";
import ProductService from "./services/product";

createConnection().then(async connection => {
    const productService = new ProductService(connection);
    const webserver = server.create(productService);
    server.setupRoutes(webserver);
    server.start(webserver);
}).catch(error => console.log("Error: ", error));