import { Product } from "../entity/Product";
import { Connection, Repository, Like } from "typeorm";

interface ProductFilters {
    '_start'?: number;
    '_limit'?: number;
    'name_contains'?: string;
}

class ProductService {
    private productRepo: Repository<Product>;

    constructor(connection: Connection){
        this.productRepo = connection.getRepository(Product);
    }

    public findProducts = async (filters: ProductFilters) => {
        const { productRepo } = this;
        const query = {
            take: Number(filters._limit),
            skip: Number(filters._start),
        }
        return await productRepo.find(filters.name_contains ? {
            ...query,
            where: {
                name: Like(`%${filters.name_contains}%`),
            }
        } : query);
    }
    public countProducts = async (filters: ProductFilters) => {
        const { productRepo } = this;
        return await productRepo.count({
            where: {
                name: Like(`%${filters.name_contains}%`),
            },
        });
    }
}

export default ProductService;