const restify = require('restify');
const cookieParser = require('restify-cookies');
const corsMiddleware = require('restify-cors-middleware')

import ProductsRoutes from "./routes/products";
import ProductService from "./services/product";

export interface injectedServices {
    productService: ProductService;
}
const cors = corsMiddleware({
    origins: ['*'],
})


export default {
    create: (productService: ProductService) => {
        const server = restify.createServer({
          name: 'ammo-api',
          version: '1.0.0'
        });
        server.use((req, res, next) => {
            req.injected = {
                productService
            } as injectedServices;
            return next();
        });
        server.pre(cors.preflight);
        server.use(cors.actual);
        server.use(restify.plugins.acceptParser(server.acceptable));
        server.use(restify.plugins.queryParser());
        server.use(restify.plugins.bodyParser());
        server.use(cookieParser.parse);
        return server;
    },
    setupRoutes: (server) => {
        server.pre(function(req, res, next) {
            req.headers.accept = 'application/json';
            return next();
        });

        server.get('/products', ProductsRoutes.list)
        server.get('/products/count', ProductsRoutes.count)
    },
    start: (server) => {
        server.listen(process.env.PORT || 9000, function () {
            console.log('%s listening at %s', server.name, server.url);
        });
    }
}