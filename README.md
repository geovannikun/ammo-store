# ammo-store

## Run backend
```
cd backend;
npm i;
npm start;
```

### To insert data into database (you need to run backend before to create db)
```
cd backend;
sqlite3 database.sqlite < seed.sql;
```

## Run frontend
```
cd frontend;
npm i;
npm start;
```

