import { observable, action, autorun } from 'mobx';
import { Product } from '../models';
import { ProductsService } from '../services';

export default class ProductsStore {
    @observable products: Product[];
    @observable productsCount: number;
    @observable search: string;
    @observable pageNumber: number;
    @observable itensPerPage: number;

    constructor() {
        autorun(() => {
            ProductsService.search(this.search, this.pageNumber, this.itensPerPage);
        })
        this.initialize();
    }

    @action private initialize() {
        this.products = [];
        this.search = "";
        this.pageNumber = 1;
        this.itensPerPage = 16;
        this.productsCount = 0;
    }

    @action setSearch(search: string): any {
        this.search = search;
    }

    @action setPageNumber(pageNumber: any): any {
        this.pageNumber = pageNumber;
    }

    @action setProducts(products: Product[], productsCount: number) {
        this.products = products;
        this.productsCount = productsCount;
    }

    @action setItensPerPage(itensPerPage: number) {
        this.itensPerPage = itensPerPage;
    }
}