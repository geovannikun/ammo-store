import ProductsStore from './products.store';

const productsStore = new ProductsStore();

export {
    productsStore,
}