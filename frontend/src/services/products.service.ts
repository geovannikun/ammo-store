import { productsStore } from '../stores';
import { Product } from '../models';

const createFilter = (search, pageNumber, itensPerPage) => {
    let filter = {
        '_start': (pageNumber - 1) * itensPerPage,
        '_limit': itensPerPage,
    } as any;

    filter = (search ? {
        ...filter,
        'name_contains': search,
    } : filter);
    return Object.keys(filter).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(filter[k])}`).join('&');
}

class ProductsService{
    static async search(search: string, pageNumber: number, itensPerPage: number) {
        const products = await fetch(`process.env.API_URL/products?${createFilter(search, pageNumber, itensPerPage)}`).then((res) => res.json());
        const productsCount = await fetch(`process.env.API_URL/products/count?name_contains=${search}`).then((res) => res.json());
        productsStore.setProducts(products.map(product => ({
            ...product,
            url: `/product/${product.id}`,
        } as Product)), productsCount.products);
    }
}

export default ProductsService;