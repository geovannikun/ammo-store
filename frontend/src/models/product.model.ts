export default interface Product {
    id: number;
    name: string;
    url?: string;
    description: string;
    price: number;
    currentPrice?: number;
    pictures: string[];
}