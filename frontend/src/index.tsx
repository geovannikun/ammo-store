import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {
  BrowserRouter
} from 'react-router-dom'
import * as mobx from 'mobx';

import App from './App';
import './assets/scss/index.scss';
import { productsStore } from './stores';
import { Provider } from 'mobx-react';
import { setStatefulModules } from 'fuse-box/modules/fuse-hmr';

setStatefulModules((name) => {
    return /stores/.test(name);
});

mobx.configure({ enforceActions: 'always' });

ReactDOM.render(
    <Provider productsStore={productsStore}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
, document.getElementById('root'));