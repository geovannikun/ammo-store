import * as React from "react";
import { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { SearchPage } from './screens';

class App extends Component {
    render() {
        return (
            <Switch>
                <Route exact path='/' component={SearchPage} />
                <Route path='/search' component={SearchPage} />
            </Switch>
        );
    }
}

export default App;