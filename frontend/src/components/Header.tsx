import * as React from "react";
import { Component } from 'react';
import { observer } from 'mobx-react';
import '../assets/scss/Header.scss';
import * as logo from '../assets/img/logo.svg';
import * as searchIcon from '../assets/img/search.svg';
import * as clearIcon from '../assets/img/clear.svg';

interface HeaderProps {
    search: string;
    onChange: (search: string) => void;
}

@observer
export default class Header extends Component<HeaderProps> {
    onSearchChange = (event) => {
        this.props.onChange(event.target.value);
    }

    render() {
        return (
            <div className='header'>
                <img src={logo}></img>
                <div className='search-box'>
                    <img src={searchIcon} className='search-box-icon'/>
                    <input placeholder="Pesquisar" defaultValue={this.props.search} onChange={this.onSearchChange}/>
                    <img src={clearIcon} className='search-box-clean'/>
                </div>
            </div>
        );
    }
}