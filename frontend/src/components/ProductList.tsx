import * as React from "react";
import { Component } from 'react';
import { observer } from 'mobx-react';

import '../assets/scss/SearchPage.scss';
import { Product } from '../models';
import '../assets/scss/ProductList.scss';

interface ProductListProps {
    products: Product[];
    productsCount: number;
}

@observer
export default class ProductList extends Component<ProductListProps> {

    asPrice = (price) => `R$ ${price.toFixed(2)}`;

    getPictures = (productId, pictures) => pictures.map((picture, index) => (
        <img src={picture} key={`pricture-${productId}-${index}}`}/>
    ));

    getPrice = (product) => product.current_price ? (
        <p className='price-tag'>
            <span>{ this.asPrice(product.price) }</span> por { this.asPrice(product.current_price) }
        </p>
    ) : (
        <p className='price-tag'>
            { this.asPrice(product.price) }
        </p>
    )

    render() {
        const { products, productsCount } = this.props;
        return (
            <div className='product-list'>
                <p>{ `${ productsCount } produtos encontrados` }</p>
                { Boolean(this.props.products.length) && (
                    <ul>
                        { this.props.products.map(product => (
                            <li key={ product.id }>
                                <div>
                                    { this.getPictures(product.id, product.pictures) }
                                    <span>
                                        <p>{ product.name }</p>
                                        <small>{ product.description }</small>
                                    </span>
                                </div>
                                { this.getPrice(product) }
                            </li>
                        )) }
                    </ul>
                )}
            </div>
        );
    }
}