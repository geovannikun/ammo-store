import * as React from "react";
import { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Header, ProductList } from '../components';
import '../assets/scss/SearchPage.scss';
import ProductsStore from '../stores/products.store';
import Pagination from 'react-js-pagination';

interface InjectedProps {
    productsStore: ProductsStore;
}

@inject('productsStore')
@observer
export default class SearchPage extends Component<any> {

    productsPerPage = [16, 32, 64];

    private get injected() {
        return this.props as InjectedProps;
    }

    changeSearch = (search: string) => {
        this.injected.productsStore.setSearch(search);
    }

    changeItensPerPage = (event) => {
        this.injected.productsStore.setItensPerPage(event.target.value);
    }
    

    changePageNumber = (pageNumber: number) => {
        this.injected.productsStore.setPageNumber(pageNumber);
    }

    render() {
        const { search, products, productsCount } = this.injected.productsStore;
        return (
            <>
                <Header search={ search } onChange={ this.changeSearch }/>
                <main className="search-page">
                    <h1>{ search || 'Lista de produtos' }</h1>
                    <ProductList products={ products } productsCount={productsCount}/>
                    <div className="search-pagination">
                        <select onChange={this.changeItensPerPage}>
                            {this.productsPerPage.map(productPerPage => (
                                <option key={`ppp-${productPerPage}`} value={productPerPage}>
                                    {`${productPerPage} produtos por página`}
                                </option>
                            ))}
                        </select>
                        <Pagination
                            activePage={this.injected.productsStore.pageNumber}
                            itemsCountPerPage={this.injected.productsStore.itensPerPage}
                            totalItemsCount={productsCount}
                            pageRangeDisplayed={6}
                            onChange={this.changePageNumber}
                            />
                    </div>
                </main>
            </>
        );
    }
}